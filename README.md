# Install and use Wireguard on AWS
These scripts automate the installation and use of Wireguard on AWS with Ubuntu Server 20.04

## How to use

### Installing
```bash
git clone https://gitlab.com/sotilrac/wireguard-aws
cd wireguard-aws
sudo ./initial.sh
```
Once the installation is complete, make sure the Wireguard port you selected during the installation is open in the AWS instance security group. See the sample rule bellow:

| Type       | Protocol | Port range | Source    | Description - optional |
| ---------- | -------- | ---------- | --------- | ---------------------- | 
| Custom UDP | UDP      | 54321      | 0.0.0.0/0 | wireguard              |

The `initial.sh` script removes the previous Wireguard installation (if any) using the `remove.sh` script. It then installs and configures the Wireguard service using the `install.sh` script. Finally, it creates a client using the `add-client.sh` script.

### Adding a new client
`add-client.sh` - Script to add a new VPN client. It creates a configuration file ($CLIENT_NAME.conf) on the path ./clients/$CLIENT_NAME/ and displays a QR-code with the configuration.

```bash
sudo ./add-client.sh
#OR
sudo ./add-client.sh $CLIENT_NAME
```

### Reseting clients
`reset.sh` - Script that removes information about clients and stops the Wireguard VPN server.
```bash
sudo ./reset.sh
```

### Uninstalling Wireguard
```bash
sudo ./remove.sh
```
## Contributors  (alphabetically)
- [Alexey Chernyavskiy](https://github.com/alexey-chernyavskiy)
- [Carlos Asmat](https://asmat.ca)
- [Max Kovgan](https://github.com/mvk)
