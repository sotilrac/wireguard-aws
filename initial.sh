#!/bin/bash

echo "# Wireguard Initial Setup"
echo "# Installing Wireguard"
./remove.sh
./install.sh
echo "# Wireguard installed"

echo "# Adding First Client"
./add-client.sh
echo "# First Client Added"

echo "# Initial Setup Complete"
